var express = require('express'),
    router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.redirect('/ui/user');
});

router.get('/ui/*', function(req, res) {
  res.render('index', { title: 'Ampex', token: 'req.csrfToken()'});
});

module.exports = router;
