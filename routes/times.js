"use strict"

var router = require('express').Router(),
    _ = require('lodash'),
    db = require('./db'),
    moment = require('moment-timezone'),
    bayeux = require('./bayeux'),
    respond = function(res, ref) {
      bayeux.getClient().publish('/channel', {
        val: 'reload',
        ref: ref
      });
      res.json({result: 'ok'})
    };


router.get('/', function(req, res) {
  var sql = "select id, start, stop, trim(user_ref) as user_ref from times where start > now()",
      withDetails = req.query.details == 'true';

  if (withDetails && !req.isAuthenticated()) {
    return res.status(401).end();
  }

  if (withDetails)
    sql = "select t.id, start, stop, trim(user_ref) as user_ref, msg, t.created_at, name, surname, email, phone, insurance "
    + " from times t left outer join users on t.user_ref = users.ref where start > now()";
  
  db.exec(sql)
    .then( r => res.json(r.rows))
    .catch( err => db.error(err, res));
});

router.post('/', function(req, res) {
  var fields = _.keys(req.body),
      values = _.values(req.body);
  
  fields.push('created_at');
  values.push(moment().format());

  var sql =  _.template(
    "insert into times (${vars}) values (?,?,?,?,?)"
  )({'vars': fields.join()});
  

  db.exec(sql, values)
    .then(() => respond(res, req.body.user_ref))
    .catch( err => db.error(err, res));
});

router.delete('/', function(req, res) {
  var row, start, stop;
  
  if (req.body.id) {
    db.exec("delete from times where id = ?", [req.body.id])
      .then(() => respond(res, req.body.ref))
      .catch( err => db.error(err, res));
  }
  else if (req.isAuthenticated) {
    start = req.body.start;
    stop = req.body.stop;
    
    // to do: use transaction
    db.exec("select id, start, stop from times where start <= ? and stop >= ?", [start, stop])
      .then(function(r) {
        row = r.rows[0];
        return db.exec("delete from times where id = ?", [row.id])
          .then(function() {
            if (moment(row.start) < moment(start)) {
              return db.exec(
                "insert into times (user_ref, start, stop) values (?,?,?)",
                ['admin', row.start, start]
              )
            } 
          })
          .then(function() {
            if  (moment(stop) < moment(row.stop)) {
              return db.exec(
                "insert into times (user_ref, start, stop) values (?,?,?)",
                ['admin', stop, row.stop]
              )
            }
          })
      })
      .then(() => respond(res, req.body.ref))
      .catch( err => db.error(err, res));
  }          
});


module.exports = router;
  
