var router = (require('express')).Router(),
    passport = require('passport'),
    _ = require('lodash'),
    db = require('./db'),
    moment = require('moment-timezone');



router.get('/', function(req, res) {
  if (req.query.email) {
    db.exec("select * from users where email=?", [req.query.email])
      .then( r => res.json(r.rows.length > 0 ? r.rows[0] : 'not found'))
      .catch( err => db.error(err, res));
  }
  else if (req.isAuthenticated()) {
     db.exec("select * from users", [])
      .then(function(r) {
        var users = _.map(r.rows, function(row) {
          return [
            row.name + ' ' + row.surname,
            row.email,
            row.phone,
            row.insurance
          ]
        });
        res.json({users: users});
      })
      .catch( err => db.error(err, res));
  }
  else
    return res.status(401).end(); 
});


router.post('/login', passport.authenticate('json'), function(req, res) {
  res.json({result: 'ok'})
});


router.post('/', function(req, res) {
  var sql,
      fields =  _.keys(req.body),
      vals = _.values(req.body),
      ref = (require('uuid')).v4().slice(0,8);

  if (fields.length != 5)
    res.json({result: 'fail', msg: 'Bad request'});
  
  fields = fields.concat(['ref', 'created_at']);
  sql = _.template(
    "insert into users (${vars}) values (?,?,?,?,?,?,?)"
  )({'vars':fields.join()});
  vals = vals.concat([ref, moment().format()]);
  db.exec(sql, vals)
    .then( () => db.exec("select * from users where ref=?", [ref]))
    .then( r => res.json({result: 'ok', user: r.rows[0]}))
    .catch( err => db.error(err, res))
});

router.put('/', function(req, res) {
  var sql = "update users set ";
  
  _.each(req.body, function(val, key) {
    if (key != 'ref')
      sql += key + " = '" + val + "',";
  });
  sql += "updated_at = '" + moment().format() + "'"; 
  sql += " where ref = '" + req.body.ref + "'";
  
  db.exec(sql)
    .then( () => db.exec("select * from users where ref=?", [req.body.ref]))
    .then( r => res.json({result: 'ok', user: r.rows[0]}))
    .catch( err => db.error(err, res));
         
});
  

module.exports = router;
