drop table users;

create table users (
id serial primary key,
ref  char(8),
name varchar(20),
surname varchar(20),
email   varchar(40),
phone   varchar(20),
insurance varchar(20),
created_at timestamptz,
updated_at timestamptz 
);

drop table times;

create table times (
id serial primary key,
user_ref char(8),
start timestamptz,
stop   timestamptz,
msg  text,
created_at timestamptz
);
