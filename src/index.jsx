var ReactDOM = require('react-dom'),
    React = require('react'),
    router = require('react-router'),
    $ = require('jquery'),
    User = require('./user.jsx'),
    Admin = require('./admin.jsx'),
    Booker = require('./booker.jsx'),
    Patients = require('./patients.jsx'),
    Router = router.Router,
    Route = router.Route;

$(function() {

  $.ajaxSetup({
    beforeSend: function(xhr) {
      var token = $('meta[name="csrf-token"]').attr('content');
      xhr.setRequestHeader( 'X-CSRF-Token', token );
    }
  });


  ReactDOM.render((
    <Router history={router.browserHistory} >
      <Route path="/ui/user" component={User} />
      <Route path="/ui/:ref/booker" component={Booker} />
      <Route path="/ui/admin" component={Admin} />
      <Route path="/ui/patients" component={Patients} />
    </Router>
  ), document.getElementById('app'));

});
