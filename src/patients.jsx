var React = require('react'),
    lib = require('./lib.jsx'),
    Alerts = require('./alerts.jsx'),
    Table = require('./table.jsx'),
    Link = require('react-router').Link;

module.exports = React.createClass({
  mixins: [Alerts],

  getInitialState: function() {
    return {
      loading: false,
      users: null,
      alerts: {}
    }
  },

  componentWillMount: function() {
    lib.get('/users', this);
  },

  render: function() {
    if (this.state.loading)
      return (<div> Loading ... </div>);

    return (
      <div className="container">
        { this.renderAlerts() }
        <Link className="btn btn-primary" to={'/ui/admin/booker'}>Schedule</Link>
        <p></p>
        { _.isEmpty(this.state.users) ? null :
          <Table head={['Name', 'Email', 'Phone', 'Insurance']}
                 body={this.state.users} search sort
          />
        }
      </div>
    );
  },

  getData: function(resp) {
    this.setState({users: resp.users});
  }

});
      
