var React = require('react'),
    moment = require('moment-timezone'),
    _ = require('lodash'),
    lib = require('./lib.jsx'),
    Link = require('react-router').Link,
    Alerts = require('./alerts.jsx');


module.exports = React.createClass({
  mixins: [Alerts],
  
  getInitialState: function() {
    var ref = this.props.params.ref;
    return {
      slot: null,
      mode: 'Week',
      offset: 0,
      diff: 1,
      start: null,
      ref: ref,
      refTime: null,
      isAdmin: ref == 'admin',
      slots: null,
      data: null,
      days: [],
      loading: false,
      alerts: {},
      subs: null
    }
  },

  componentWillMount: function() {
    this.fetch();
  },

  componentDidMount: function() {
    var client = new Faye.Client('/faye', {timeout: 120}),
        subs = client.subscribe('/channel', _.bind(function(msg) {
          if (msg.val == 'reload')
            this.onSaved();
        }, this));

    this.setState({subs: subs});
  },

  componentWillUnmount: function() {
    this.state.subs.cancel();
  },

  render: function() {
    if (this.state.loading)
      return (<div> Loading ... </div>);

    return (
      <div className="container">
      { this.renderAlerts() }
      { this.state.slots ? this.renderSlots() : null}
      </div>
    );
  },

  renderSlots: function() {
    var width = this.state.action ? 8 : 12;
    
    return (
      <div>
        <div className="form-group col-md-12">
          {this.renderOffset()}
          { this.state.isAdmin ? (
              <div>
                {this.renderMode()}
                <Link className="btn btn-primary" to={'/ui/patients'}>Patients</Link>
              </div>
            ) : null
          }             
        </div>
      {this.state.action  ? this.renderForm() : null}
      <div className={"col-md-" + width}>    
        <table className='table table-bordered'>
          <thead>
            <tr>
              <th style={{width: '10%'}}></th>
              {
                 _.map(this.state.days, _.bind(function(day, idx) {
                   return this.showDay(day, idx);
                 }, this))
              }
            </tr>
          </thead>
          <tbody onClick={this.cellClick}>
            {
               _.map(this.state.slots, _.bind(function(row, idx) {
                 return  this.showRow(row, idx);
               }, this))
            }
          </tbody>
        </table>
      </div>

    
     </div>
    );
  },

  renderForm: function() {
    var action = this.state.action,
        isCancel = _.includes(lib.cancelActions, action);
        mode = isCancel ? 'danger' : 'primary',
        slot = this.getSlot(),
        down = this.state.index.row;
    
    return  (
      <div className="col-md-4">
        { _.map(_.range(down), function() {                                         
            return <br></br>
          })
        }
        
        {this.renderInfo(slot.time)}
        <form className="form-horizontal">
          { action == 'book' ?  this.renderReason() :
            action != 'cancel' ? this.renderStop(slot) : null
          }
          {this.renderButtons(mode, isCancel)}
        </form>
      </div>
    );
  },

  renderInfo: function(time) {
    var clock = time.format('h:mm A'),
        action = this.state.action;
    
    if (action != 'book' && action != 'cancel')
      return <p>{action + 'ing from ' + clock + ' to'}</p>;
    else
      return <p>{'You are ' + action + 'ing appointment for ' + clock}</p>;
  },

  renderReason: function() {
    return (
      <div className="form-group">
        <label htmlFor="reason">Reason for visit:</label>
        <textarea className="form-control" rows="5" ref="msg"/>
      </div>
    );
  },

  renderStop: function(slot) {
    var slots = lib.leftSlots(slot, this.state.data),
        val = slots[0];
    return (
      <div className="form-group">
      <select className="form-control" defaultValue={val} ref='stop' >
      { _.map(slots, function(slot, idx) {
        return (<option key={idx} value={slot}>{slot.format('h:mm A')}</option>)
      })
      }
      </select>
      </div>
    );
  },
       
  renderButtons: function(mode, isCancel) {
    var name = _.startCase(this.state.action),
        fun = isCancel ? this.cancel : this.book;

    return (
      <div>
        <button type="submit" className={"btn btn-" + mode} onClick={fun}>
          {name}
        </button>
        <button type="submit" className="btn btn-default" onClick={this.undo}>
          Undo
        </button>
      </div>
    );
  },

  renderMode: function() {
    return (
      <div className="col-md-2">
        {_.map(["Week","Day"], _.bind(function(mode) {
           return (
             <label class="radio-inline">
               <input type="radio" value={mode} checked={this.state.mode === mode}
                      onChange={this.modeChanged}/>
               {mode}&nbsp;
             </label>
           );
         }, this))
        }
      </div>
    );
  },

  renderOffset: function() {
    return (
      <div>
        {_.map(["left", "right"], _.bind(function(dir) {
          return (
            <div className="col-md-1">
            <button type="button" onClick={this.dirClick.bind(null, dir)}>
            <span className={"glyphicon glyphicon-triangle-" + dir}></span>
            </button>
            </div>
          );
         }, this))
         }
      </div>
    );
  },

  
  showDay: function(day, idx) {
    return (<th key={idx}>
        { moment(moment(this.state.start).add(day,'d')).format("ddd, MMM D") }
    </th>);
  },

  showRow: function(row, m) {
    return (
      <tr key={m}>
        <td key='0'>{moment(row[0].time).format("h:mm A") }</td>
        {
          _.map(row, function(slot, n) {
            return (<td key = {n+1} className={slot.mode}>{slot.desc}</td>);
          })
         }
      </tr>
    )
  },
  
  cellClick: function(e) {
    var index = {
          row: e.target.parentElement.rowIndex - 1,
          cell: e.target.cellIndex - 1
          },          
        slot = this.state.slots[index.row][index.cell],
        action = lib.getAction(slot, this);
      
    if (!action)
      return;
    
    if (action == 'over')
      this.setAlert('info', 'Appointment for today can only be canceled by phone');
    else {
      slot.mode = 'warning';
      this.setState({
        action: action,
        index: index,
      });
    }
  },
  
  modeChanged: function(ev) {
    var state;
    if (ev.currentTarget.value != this.state.mode) {
      state = _.clone(this.state);
      state.mode = ev.currentTarget.value;
      state.offset = 0;
      state.diff = 1;
      this.setState(lib.setSlots(state));
    }
  },

  dirClick: function(dir) {
    var state = _.clone(this.state);
    state.diff = dir === 'left' ? -1 : 1;
    state.offset += state.diff;
    this.setState(lib.setSlots(state));
  },

  book: function(e) {
    var slot = this.getSlot(),
        stop = this.refs.stop ? moment(this.refs.stop.value) :
               moment(slot.time).add(lib.slot, 'm'),
        data = { 
          msg: this.refs.msg ? this.refs.msg.value : null,
          start: moment(slot.time).format(),
          stop: stop.format(),
          user_ref: this.state.ref
        };
    
    e.preventDefault();
    lib.save('/times', 'post', data, this);
  },

  cancel: function(e) {
    var query = { ref: this.state.ref},
        slot = this.getSlot();   

    if (slot.id) {
      query.id = slot.id;
      query.ref = slot.ref;
    }
    else {
      query.start = moment(slot.time).format();
      query.stop = moment(new Date(this.refs.stop.value)).format();
    }
        
    e.preventDefault();
    lib.save('/times', 'delete', query, this);
  },

  undo: function(e) {    
    var slot = this.getSlot();
    e.preventDefault();

    slot.mode = lib.state2mode[slot.state];    
    this.setState({action: null});
  },

  onSaved: function(resp) {
    this.setState({
      action: null,
      slot: null,
      slots: null,
      data: null,
      refTime: null,
      saved: true
    });
    this.fetch();
  },

  fetch: function() {
    lib.get('/times?details=' + this.state.isAdmin, this);
  },

  getData: function(resp) {
    var state = _.clone(this.state);

    moment.tz.setDefault(lib.zone);    
    state.data = {}; 
    
    _.each(resp, function(entry) {
      lib.setEntry(entry, state);
    });

    this.setState(lib.setSlots(state));
  },
  
  getSlot: function() {
    return this.state.slots[this.state.index.row][this.state.index.cell];
  },

                                   
});

