var $ = require('jquery'),
    moment = require('moment-timezone'),
    lib = exports,
    conf = {
      hours: 11,
      startHour: 9,
      workHours: 7,
      days: 4,
    };

lib.state2mode = {
  free: 'success',
  booked: 'info',
  reserved: 'danger',
  past: 'danger',
  spare: 'danger'
};

lib.cancelActions = [
  'cancel',
  'unblock',
  'withdraw'
];


lib.slot = 20;
lib.zone = 'America/New_York';

lib.request = function(config) {
  if (!config.method)
    config.method = 'get';
  if (_.isUndefined(config.contentType)) {
    config.contentType = 'application/json';
    config.dataType = 'json';
  }

  config.url = '/api' + config.url
  config.beforeSend = function(jqxhr, config) {jqxhr.requestURL = config.url};
  return $.ajax(config).promise()
};

lib.get = function(url, caller) {
  caller.setState({loading: true});
  this.request({url: url})
      .done(function(resp) {
        if (resp.result == 'fail')
          caller.setError(resp.msg);
        else
          caller.getData(resp)
      })
      .fail(function(error) {
        caller.handleError(error);
      })
      .always(function() {
        caller.setState({loading: false})
      })
};

lib.save = function(url, method, data, caller) {
  caller.setState({saved: false});
  this.request({
    url: url,
    method: method,
    data: JSON.stringify(data)
  })
      .done(function(resp) {
        if (resp.result == 'ok') {
          if (!caller.state.saved)
	          caller.onSaved(resp);
	      }
        else
          caller.setError(resp.msg);
      })
      .fail(function(error) {
        caller.handleError(error);
      })
};



lib.setSlots = function(st) {
  var now,
      over,
      day,
      shift,
      nSlots = 60/lib.slot*conf.hours;

  moment.tz.setDefault(lib.zone);
  now = moment();
  over = moment().add(1, 'd').startOf('day');

  if (st.mode == 'Week') {
    if (now.isoWeekday() > 4 && st.offset == 0)
      st.offset = 1;

    if (!st.isAdmin && st.refTime && st.offset === 0)
      st.start = st.refTime.startOf('isoWeek');
    else
      st.start = moment().startOf('isoWeek').add(st.offset,'w');

    st.days = _.range(conf.days);
  }
  else {
    st.start = moment().startOf('day').add(st.offset,'d');
    day = st.start.isoWeekday();
    if (day > 4) {
      shift = st.diff > 0 ? 8 - day : 4 - day;
      st.start.add(shift, 'd');
      st.offset += shift;
    }

    st.days = [moment(st.start).diff(st.start.startOf('isoWeek'), 'd')];
  }

  st.start.add(conf.startHour,'h');

  st.slots =
  _.chain(_.range(nSlots))
   .map(function(n) {
     var t0 = moment(st.start).add(n*lib.slot, 'm');
     return _.chain(st.days).map(function(d) {
       return lib.fillSlot(moment(t0).add(d,'d'), over, st)
     }).value();
   })
   .value();

  return st;
};

lib.fillSlot =  function(t, over, state) {
  var isEvening = lib.isEvening(t),
      entry = state.data[t],
      s = {
        state: 'free',
        desc: '',
        time: t,
      };

  if (entry) {
    s.state = 'reserved';
    if (isEvening && entry.user_ref == 'admin' && t > over) {
      s.state = 'free';
      s.end = entry.end || moment(entry.stop);
    }
    else if (state.isAdmin) {
      s.desc = lib.fillDesc(entry, state.mode);
      s.id = entry.id;
      s.ref = entry.user_ref;
      if (entry.user_ref != 'admin')
        s.state = 'booked';
      s.end = entry.end || moment(entry.stop);
      // end is pre-assigned for blocked slots
    }
    else if (entry.user_ref == state.ref) {
      s.state = 'booked';
      s.id = entry.id;
      s.desc = 'Your appointment'
    }
  }
  else if (t < over)
    s.state = 'past';
  else if (isEvening)
    s.state = 'spare';

  s.mode = lib.state2mode[s.state];
  return s;
};

lib.blockSlots = function(start, stop, duration, data) {
  _.each(_.range(duration/lib.slot), function(n) {
    var t = moment(start).add(n*lib.slot, 'm');
    if (!data[t]) {
      data[t] = {
        user_ref: 'admin',
        start: t,
        end: stop
      }
    }
  });
};


lib.fillDesc = function(d, mode) {
  var name = d.name + ' ' + d.surname,
      time = d.created_at ? moment(d.created_at).format('MMM D h:mm A') : ' ';

  if (d.user_ref == 'admin')
    return '';

  if (mode == 'Week')
    return name;
  else
    return _.join([name, d.phone, d.email, d.insurance, d.msg, time], '; ');
};

lib.setEntry = function(entry, state) {
  var start = moment(entry.start),
      stop = moment(entry.stop),
      duration = stop.diff(start, 'm');

  if (!state.isAdmin && entry.user_ref == state.ref)
    state.refTime = start;

  if (entry.user_ref != 'admin')
    state.data[start] = entry;
  else
    lib.blockSlots(start, stop, duration, state.data);
},

lib.leftSlots = function(slot, data) {
  var stopHour = conf.startHour + (lib.isEvening(slot.time) ? conf.hours : conf.workHours),
      stop = slot.end || moment(slot.time).startOf('day').add(stopHour, 'h'),
      slots = _.range(stop.diff(slot.time, 'm')/lib.slot);


  if (slot.state == 'spare') {
    _.each(slots, function(idx) {
      var t = moment(slot.time).add((idx + 1)*lib.slot, 'm');
      if (data[t]) {
        stop = t;
        return false;
      }
    });
    slots = _.range(stop.diff(slot.time, 'm')/lib.slot);
  }


  return _.map(slots, function(idx) {
    return moment(slot.time).add((idx + 1)*lib.slot, 'm');
  });
}

lib.isEvening = function(t) {
  var workEnd = moment(t).startOf('day').add(conf.startHour + conf.workHours, 'h'),
      dayEnd = moment(t).startOf('day').add(conf.startHour + conf.hours, 'h');
  return t >= workEnd && t < dayEnd;
}

lib.getAction = function(slot, caller) {
  if (!slot || slot.state == 'past')
    return;

  if (caller.state.isAdmin) {
    switch (slot.state) {
      case 'reserved':
        return 'unblock';
      case 'free':
        return lib.isEvening(slot.time) ? 'withdraw' : 'block';
      case 'spare':
        return 'extend';
      case 'booked':
        return 'cancel';
    };
  }
  else if (slot.state == 'free') {
    if (!caller.state.refTime)
      return 'book';
    else
      caller.setAlert('info', 'You may book only one appointment');
  }
  else if (slot.state == 'booked') {
    if (slot.time < moment().add(1, 'd').startOf('day') && !caller.state.isAdmin)
      return 'over';
    else
      return 'cancel';
  }
};
