var React = require('react'),
    ReactDOM = require('react-dom'),
    Link = require('react-router').Link,
    _ = require('lodash'),
    lib = require('./lib.jsx'),
    Alerts = require('./alerts.jsx');

module.exports = React.createClass({
  mixins: [Alerts],

  getInitialState: function() {
    return {
      email: null,
      loading: false,
      alerts: {},
      user: {},
    }
  },

  render: function() {
    if (this.state.loading)
      return (<div> Loading ... </div>);
    return (
      <div>
      { this.renderAlerts() }
      { this.state.email && _.isEmpty(this.state.alerts.error) ? this.renderUser() : this.renderEmail() }
      </div>
    );
  },

  renderEmail: function() {
    return (
      <form className="form-inline">
        <div className="form-group">
          <label htmlFor="email">Email:&nbsp;</label>
          <input type="email" className="form-control" ref="email" required/>
          &nbsp;
          <button type="submit" className="btn btn-primary" onClick={this.find}>Submit</button>
        </div>        
      </form>
    );
  },

  renderUser: function() {
    var isNew =  _.isEmpty(this.state.user);
    return (
      <div className="fluid-container">
        
        <form className="form-horizontal" >
          {this.renderForm('name', 'First name', 'text')}
          {this.renderForm('surname', 'Last name', 'text')}
          {this.renderForm('email', 'Email', 'email', this.state.email)}
          {this.renderForm('phone', 'Phone', 'tel')}
          {this.renderForm('insurance', 'Insurance', 'text')}
          { isNew ? null : (
              <div className="col-sm-offset-1 col-sm-1">
                <Link className="btn btn-primary" to={'/ui/' + this.state.user.ref + '/booker'}>
                  Next
                </Link>
              </div>
            )
          }
              { this.state.saved ? null : (
                  <div className="col-sm-offset-2 col-sm-1">
                    <button type="submit" className="btn btn-primary" onClick={_.partial(this.save, isNew)}>
                      Save
                    </button>
                  </div>
                )
              }
        </form>
      </div>
    )
  },

  renderForm: function(name, label, type, val) {
    if (!val)
      val = this.state.user[name];
    return (
      <div className="form-group">      
        <label forHtml={name} className="control-label col-sm-2">{label + ':'}</label>
        <div className="col-sm-4">
        <input type={type} className="form-control" id={name} ref={name} name={name}
               defaultValue={val} required />
        </div>
      </div>
    );
  },
    
  find: function(e) {
    var self = this, 
	      email = this.refs.email.value.trim();
    e.preventDefault();
    this.clearAlerts();
    this.setState({email: email});
    lib.get("/users?email=" + email, this);
  },

  getData: function(resp) {
    if (resp !== 'not found') {
      this.setAlert(
        'info',
        "If your data is correct press Next, you can also update it and press Save"
      );
      this.setState({user: _.clone(resp)});
    }
    else
      this.setAlert('info', "Fill your personal data and press Save");      
  },

  save: function(isNew, e) {
    var data = {},
        method = isNew ? 'post' : 'put';

    e.preventDefault();
    if (!isNew)
      data.ref = this.state.user.ref;
    _.forOwn(this.refs, function(val, prop) {
      if (val.value)
        data[prop] = val.value;
    });
    lib.save('/users', method, data, this);
  },

  onSaved: function(resp) {
    this.setState({
      user: _.clone(resp.user),
      saved: true
    });
    this.setAlert('success', 'Your data is saved, press Next to proceed');
  }
	
});
