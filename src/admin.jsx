var React = require('react'),
    ReactDOM = require('react-dom'),
    Link = require('react-router').Link,
    lib = require('./lib.jsx'),
    Alerts = require('./alerts.jsx');

module.exports = React.createClass({
  mixins: [ Alerts ],
  
  contextTypes: {
    router: React.PropTypes.object.isRequired
  },

  getInitialState: function() {
    return {
      error: null,
      loading: false,
      alerts: {},
      passed: false // not used now
    }
  },
  
  render: function() {
    if (this.state.loading)
      return (<div> Loading ... </div>);
    return (
      <div className="container-fluid">
        { this.renderAlerts() }
      	{this.state.passed ? this.renderLinks() : this.renderPassword()}
         
      </div>
    );
  },

  renderPassword: function() {
    return (
      <form className="form-inline">
        <div className="form-group">
	        <label htmlFor="password">Password:&nbsp;</label>
          <input type="password" className="form-control" ref="password" required/>
	        &nbsp;
	        <button type="submit" className="btn btn-primary" onClick={this.auth}>Submit</button>
        </div>        
      </form>
    );
  },

  renderLinks: function() {
    return (
      <form className="form-horizontal">
      <div className="form-group col-sm-12">
        <div className="col-sm-offset-2 col-sm-2">
          <Link className="btn btn-primary" to={'/ui/admin/booker'}>Schedule</Link>
        </div>
        <div className="col-sm-offset-4 col-sm-2">
          <Link className="btn btn-primary" to={'/ui/admin/users'}>Patients</Link>
        </div>
      </div>
      </form>
    );
  },    

  auth: function(e) {
    e.preventDefault();
    lib.save("/users/login", 'post', {
      username: 'admin',
      password: this.refs.password.value
    }, this);
  },

  onSaved: function() {
    this.context.router.push('/ui/admin/booker');
  }
      
});
